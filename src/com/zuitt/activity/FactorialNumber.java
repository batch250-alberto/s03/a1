package com.zuitt.activity;

import java.util.Scanner;


public class FactorialNumber {
    public static void main(String[] args){
        try {
            System.out.println("Input an integer: ");
            Scanner in = new Scanner(System.in);

            int num = in.nextInt();
            int factorial = 1;

            if (num < 0) {
                System.out.println("Input a positive number!");
            } else {
                if (num == 0) {
                    System.out.println("Factorial of " + num + " is: " + factorial);
                } else {
                    for (int i = 1; i <= num; i++) {
                        factorial *= i;
                    }
                    System.out.println("Factorial of " + num + " is: " + factorial);
                }
            }
        }catch(Exception e){
            System.out.println("Input a whole number!");
            e.printStackTrace();
        }
    }
}
